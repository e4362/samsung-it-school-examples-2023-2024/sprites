package com.example.myapplication;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

public class MySurface extends SurfaceView implements
        SurfaceHolder.Callback {
    float x, y; //координаты картинки
    float tx, ty; //координаты точки касания
    float dx, dy; //смещение
    float koeff; //коэффициент смещения

    Bitmap image;
    Resources res;
    Paint paint;


    float wallX = 400, wallY = 400; //координаты стены
    Bitmap wall;

    float hs, ws; //ширина и высота экрана
    float hi, wi; //высота и ширина изображения
    boolean isFirsts = true;
    Rect wallRect, imageRect;

    DrawThread drawThread;
    GameMap gameMap;

    public MySurface(Context context) {
        super(context);
        getHolder().addCallback(this);
        x = 500;
        y = 1000;
        koeff = 20;
        res = getResources();
        image = BitmapFactory.decodeResource(res, R.drawable.apple);
        wall = BitmapFactory.decodeResource(res, R.drawable.samsung);
        hi = image.getHeight();
        wi = image.getWidth();
        paint = new Paint();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            tx = event.getX();
            ty = event.getY();
            calculate();
        }
        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isFirsts){
            hs = canvas.getHeight();
            ws = canvas.getWidth();
            gameMap = new GameMap(ws, hs, res);
            isFirsts = false;
        }
        gameMap.draw(canvas);
//        canvas.drawColor(Color.WHITE);
//        canvas.drawBitmap(image, x, y, paint);
//        canvas.drawBitmap(wall, wallX, wallY, paint);
//        if (tx != 0){
//
//        }
//        if (Math.abs(tx - x) <= 10 || Math.abs(ty - y)  <= 10){
//            return;
//        }
//
//        imageRect = new Rect((int) x, (int) y,
//                (int) (x + wi), (int) (y + hi));
//
//        wallRect = new Rect((int) wallX, (int) wallY,
//                (int) (wallX + wall.getWidth()),
//                (int) (wallY + wall.getHeight()));
//        x += dx;
//        y += dy;
//
//        if (imageRect.intersect(wallRect)){
//            dx = 0;
//            dy = 0;
//            Log.d("CRASH", "CRASH");
//        }
//
//        checkScreen();
    }

    private void checkScreen(){
        if (y + hi >= hs || y <= 0){
            dy = -dy;
            Log.d("CRASH", "-dy");
        }
        if (x + wi >= ws || x <= 0){
            dx = -dx;
            Log.d("CRASH", "-dx");
            Log.d("CRASH", x + " " + wi);

        }

    }

    private void calculate(){
        double g = Math.sqrt((tx - x)*(tx - x) + (ty - y)*(ty - y));
        dx = (float) (koeff * (tx - x) / g);
        dy = (float) (koeff * (ty - y) / g);
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        drawThread = new DrawThread(this, getHolder());
        drawThread.isRun = true;
        drawThread.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
        boolean stop = true;
        drawThread.isRun = false;
        while (stop){
            try {
                drawThread.join();
                stop = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
